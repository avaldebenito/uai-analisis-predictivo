N = 100
xt <- rep(0,N)
yt <- rep(0,N)

mu0_xt = 0 
mu0_yt = 2

sigma2_xt = 1
sigma2_yt = 2

xt[1] = rnorm(1,mu0_xt,sqrt(sigma2_xt))
yt[1] = rnorm(1,mu0_yt,sqrt(sigma2_yt))

for(t in 2:N)
{
xt[t] = rnorm(1,xt[t-1],sqrt(sigma2_xt))
yt[t] = rnorm(1,yt[t-1],sqrt(sigma2_yt))
}

ts.plot(cbind(xt,yt),col=c(1,2),lwd=c(1,4))
plot(xt,yt)

summary(lm(yt~xt))

dxt= diff(xt)
dyt= diff(yt)

ts.plot(cbind(dxt,dyt),col=c(1,2),lwd=c(1,4))
plot(dxt,dyt)

summary(lm(dyt~dxt))

