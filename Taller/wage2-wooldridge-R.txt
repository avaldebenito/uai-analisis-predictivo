## test de carga
#######################################################################
#
# Dataset: Grade Point Average (GPA)
#
#######################################################################
#
# Summary: Data on monthly earnings, education, several demographic variables, 
# and IQ scores for 935 men in 1980.
#
#  1. wage                     monthly earnings
#  2. hours                    average weekly hours
#  3. IQ                         IQ score
#  4. KWW                    knowledge of world work score
#  5. educ                     years of education
#  6. exper                    years of work experience
#  7. tenure                   years with current employer
#  8. age                       age in years
#  9. married                  =1 if married
# 10. black                    =1 if black
# 11. south                    =1 if live in south
# 12. urban                    =1 if live in SMSA
# 13. sibs                      number of siblings
# 14. brthord                 birth order
# 15. meduc                  mother's education
# 16. feduc                    father's education
# 17. lwage                   natural log of wage
#
#######################################################################
#
# Source: Wooldridge (2012) 
# Introductory Econometrics: A Modern Approach (5th edition)
# South-Western, Cengage Learning
#
#######################################################################
#
# Copyright of R code by:
#
# Hedibert Freitas Lopes 
# Professor of Statistics and Econometrics
# Insper - Institute for Education and Research
#
#######################################################################
rm(list=ls())

data = matrix(scan("wage2-wooldridge.txt"),ncol=17,byrow=TRUE)

wage  = data[,1]
iq        = data[,3]
educ   = data[,5]
exper = data[,6]
age    = data[,8]
sibs    = data[,13]
meduc = data[,15]
feduc   = data[,16]
black   = data[,10]
south   = data[,11]
urban  = data[,12]
lwage  = data[,17]




reg = lm(educ~sibs+meduc+feduc)
summary(reg)

reg = lm(wage~iq+educ+age+exper)
summary(reg)
